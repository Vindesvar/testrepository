public class TrigDemoToCalOpty {
    
    public static void CalOptyAmt (List<Opportunity> opty) {
       
        Double amt = 0;
        for(Opportunity op : [select Amount from Opportunity where createdDate = TODAY AND createdbyID =:Userinfo.getUserID()]) {
			amt += op.Amount;            
        }
        
        for(Opportunity op1 : opty) {
            amt += op1.Amount;
            
            if(amt > 10000) {
                op1.addError('Amount limit has exceeded for today' + amt);
            }
        }
    }

}