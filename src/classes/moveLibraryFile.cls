public class moveLibraryFile { 
	
	public static void move()
	{
		Id  TargetWS = [select Id, Name from contentworkspace where name = 'mylibrary'].id;
		
		List<contentworkspace> lstWS = [select id, name from contentworkspace where name = 'Deletion Library'];
		
		Set<ID>  CWSID = new Set<ID>();
		
		for (contentworkspace cs : lstWS) {
		
			CWSID.add(cs.id);
		}
		
		List<ContentDocument> LstCDoc =  [select Id,
                            ParentId
                            from ContentDocument
                            where parentid in :CWSID ];
							
							
		for (ContentDocument cdoc : LstCDoc) {
			cdoc.ParentId = TargetWS;
            system.debug('List of docs>>>>>'+ cdoc);
			}			
        update LstCDoc;
	}
 
}