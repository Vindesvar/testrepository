public class wizard_class {
    Account a = new Account();
    Contact c = new Contact();
    Opportunity o = new Opportunity ();
    
    public Account geta() {
    return a;
    }
    
    public Contact getc() {
    return c;
    }
        
    public Opportunity geto() {
    return o;
    }
        
    public PageReference gotoPage1() {    
    Return Page.Page1;
    }
    
    public PageReference gotoPage2() {    
    Return Page.Page2;
    }
    
    public PageReference gotoPage3() {    
    Return Page.Page3;
    }
    
    public PageReference gotoPage4() {    
    Return Page.Page4;
    }
    
    public PageReference Save_now() {
    insert a;
    c.AccountId = a.Id;
    insert c;
    o.AccountId = a.Id;
    insert o;
    
    Return page.Page5;
    }     
    
}