public class PageBlockTableSortingCon {
 
   private List<Account> accounts;
   private String sortDirection = 'ASC';
   private String sortExp = 'name';

   public String sortExpression
   {
     get
     {
        return sortExp;
     }
     set
     {
       //if the column is clicked on then switch between Ascending and Descending modes
       if (value == sortExp)
         sortDirection = (sortDirection == 'ASC')? 'DESC' : 'ASC';
       else
         sortDirection = 'ASC';
       sortExp = value;
     }
   }

 public String getSortDirection()
 {
    //if not column is selected 
    if (sortExpression == null || sortExpression == '')
      return 'ASC';
    else
     return sortDirection;
 }

 public void setSortDirection(String value)
 {  
   sortDirection = value;
 }
  
   public List<Account> getAccounts() {
       return accounts;
   }


   public PageReference ViewData() { 
       string sortFullExp = sortExpression  + ' ' + sortDirection;
      
       //query the database based on the sort expression
       accounts = Database.query('Select id, Name, BillingCity, BillingCountry, Phone from Account order by ' + sortFullExp + ' limit 1000');   
   return null;
  }
  public PageBlockTableSortingCon(){ 
       //build the full sort expression
       string sortFullExp = sortExpression  + ' ' + sortDirection;
      
       //query the database based on the sort expression
       accounts = Database.query('Select id, Name, BillingCity, BillingCountry, Phone from Account order by ' + sortFullExp + ' limit 1000');
       //return null;
   }
   
   
   /*           Map<String , String> mapOfCNNew = new Map<String , String>();
            Map<String , String> mapOfCNOld = new Map<String , String>();
                       
            for (Contract ct : CTR) {                                
                 mapOfCNNew.put(ct.contractNumber,ct.New_Contract_Code__c);
                 mapOfCNOld.put(ct.contractNumber,ct.Old_Contract_Code__c);
            }
            Map<string , Contract>  mapOfCNNewStatus = new Map<string , Contract>();
            for(Contract cnew : [Select Id, contractNumber, Status__c from Contract where contractNumber IN : mapOfCNNew.values()]){
                mapOfCNNewStatus.put(cnew.contractNumber,cnew);
            }    
            Map<string , Contract>  mapOfCNOldStatus = new Map<string , Contract>();
            for(Contract cnew : [Select Id, contractNumber, Status__c from Contract where contractNumber IN : mapOfCNOld.values() ]){
                mapOfCNOldStatus.put(cnew.contractNumber,cnew);
            } 
            for (Contract ct : CTR) {
                if((mapOfCNNew.containsKey(ct.contractNumber) && mapOfCNNewStatus.containsKey(mapOfCNNew.get(ct.contractNumber))) && ((mapOfCNOld.containsKey(ct.contractNumber)) && mapOfCNOldStatus.containsKey(mapOfCNOld.get(ct.contractNumber)))){                
                    /*if((mapOfCNNewStatus.get(mapOfCNNew.get(ct.contractNumber)).Status__c != 'Executed') && (mapOfCNOldStatus.get(mapOfCNOld.get(ct.contractNumber)).status__c != 'Executed')){
                        contRetainId.add(ct.id);
                    }
                    if((mapOfCNNewStatus.get(mapOfCNNew.get(ct.contractNumber)).Status__c != 'Executed') || (mapOfCNOldStatus.get(mapOfCNOld.get(ct.contractNumber)).status__c != 'Executed')){
                        contRetainId.add(ct.id);
                    }                    
                    if((mapOfCNNewStatus.get(mapOfCNNew.get(ct.contractNumber)).Status__c != 'Executed') && (ct.Old_Contract_Code__c == null && ct.Old_Contract_Code__c =='')){
                        contRetainId.add(ct.id);
                    }
                    if((ct.New_Contract_Code__c =='' && ct.New_Contract_Code__c == null) && (mapOfCNOldStatus.get(mapOfCNOld.get(ct.contractNumber)).status__c != 'Executed')){
                        contRetainId.add(ct.id);
                    } */
                   /* if((mapOfCNNewStatus.get(mapOfCNNew.get(ct.contractNumber)).Status__c != 'Executed')){
                        contRetainId.add(ct.id);
                    }
              }                 
           }*/
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   

}