public class NewAndExistingController {


 public Account account { get; private set; }
  public Account acName { get; set; }

    public NewAndExistingController() {
    Account a= new Account();
        Id id = ApexPages.currentPage().getParameters().get('id');
        system.debug('id------->>>>>>'+id+'Name>>>>>>>>>>>>>>>>>>>>>'+acName );
        account = (id == null) ? new Account() :
            [SELECT Name, Phone, Industry FROM Account WHERE Id = :id];
            system.debug('id------->>>>>>'+id+'>>>'+account );
    }
    public PageReference save() {
        try {
            upsert(account);
        } catch(System.DMLException e) {
            ApexPages.addMessages(e);
            return null;
        }
        //  After successful Save, navigate to the default view page
        PageReference redirectSuccess = new ApexPages.StandardController(Account).view();
        return null;//(redirectSuccess);
    }


}