global class GetAccountRecordsBatch implements Database.Batchable<sObject>, Database.stateful {
    global Database.QueryLocator start(Database.BatchableContext bc){
        String query = 'Select Name from Account limit 10';
        return Database.getQueryLocator(query);        
    }	
    List<Account> lstOfAccount1 = new List<Account>();
    global void execute(Database.BatchableContext bc, List<Account> lstOfAccount){
        //List<Account> lstOfAccount1 = new List<Account>();
        for(Account acc : lstOfAccount){
            lstOfAccount1.add(acc);
            //system.debug('Execute method called');
        }
        system.debug('Execute method called ' + lstOfAccount1);
    }
    
    global void finish(Database.BatchableContext bc){
        system.debug('Finish method Called' + lstOfAccount1);
    }
}