public class myControllerExtension { 

      public Account acct;  



    // The extension constructor initializes the private member 

   // variable acct by using the getRecord method from the standard 

    // controller. 
    public myControllerExtension(ApexPages.StandardController stdController) { 

        acct = (Account)stdController.getRecord(); 
        system.debug('Hello >>>>>>>>>>>>>>>>' + acct.name + ' (' + acct.id + ')');
    } 
    public String getGreeting() { 
       // system.debug('Hello >>>>>>>>>>>>>>>>' + acct.name + ' (' + acct.id + ')');
        return 'Hello ' + acct.name + ' (' + acct.id + ')'; 
    }  
}