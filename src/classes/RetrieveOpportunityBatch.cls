global with sharing class RetrieveOpportunityBatch implements Database.Batchable<sObject>, Database.stateful {
    global Database.QueryLocator start(Database.BatchableContext bc){
        String query = 'select name from opportunity';
        system.debug('Start method executed');
        return Database.getQueryLocator(query);
    }
    global void execute(Database.BatchableContext bc, List<Account> Acc){
        for(Account obj : acc){
            system.debug('Execute method executed');
        }
    }
    global void finish(Database.BatchableContext bc){
        system.debug('Finish method executed');
    }
}