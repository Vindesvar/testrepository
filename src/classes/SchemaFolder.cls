public class SchemaFolder {
	public static void docfolId()
    {
		        List<Schema.DescribeTabSetResult> tabSetDesc = Schema.describeTabs();
// Iterate through each tab set describe for each app and display the info

for(DescribeTabSetResult tsr : tabSetDesc) {
    String appLabel = tsr.getLabel();
    System.debug('Label: ' + appLabel);
    System.debug('Logo URL: ' + tsr.getLogoUrl());
    System.debug('isSelected: ' + tsr.isSelected());
    String ns = tsr.getNamespace();
    if (ns == '') {
        System.debug('The ' + appLabel + ' app has no namespace defined.');
    }

    else {
        System.debug('Namespace: ' + ns);
      }
        
    }
}
}