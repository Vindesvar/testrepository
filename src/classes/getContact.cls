@RestResource(urlMapping='/v1/getContacts/*')
   global with sharing class getContact {
     @Httpget
      global static list<contact> fetchAccount(){
        RestRequest req = RestContext.request;
        system.debug('@@@@@@@@@@req '+req );          
        RestResponse res = Restcontext.response;
        system.debug('@@@@@@@@@@res '+res );        
        Id accId = req.requestURI.substring(req.requestURI.lastIndexOf('/')+1);
        system.debug('@@@@@@@@@@accId  '+accId );
        list<contact> lstcontact =[Select id , name,Phone,Fax,Email from contact where Accountid=:accId];
        system.debug('@@@@@@@@@@accId  '+accId );
        return lstcontact ;
      }
   }