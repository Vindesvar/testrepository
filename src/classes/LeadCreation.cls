public class LeadCreation {
    public Lead objLead;
    public String lastName;
    public LeadCreation() {
        
    }
    public PageReference newLead() {
        objLead = new Lead(Company = 'Test', LastName = lastName, Status = 'Open - Not Contacted');
        try {
            insert objLead;
            PageReference pg = new PageReference('/' + objLead.Id);
            pg.setRedirect(true);
            return pg;
        } catch(DMLException e) {
            return null;
        }
    }
}