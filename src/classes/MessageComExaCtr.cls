public class MessageComExaCtr{
    Account account;
   public string txtValue{get;set;}
   
    public MessageComExaCtr(){
        txtValue = '';
    }

    public PageReference save() {
    try{
        if(txtValue!=null && txtValue != '')
        update account;
    }
    catch(DmlException ex){
        ApexPages.addMessages(ex);
    }
    return null;
    }

    public String getName() { 
        return 'MyController';
    }

    public Account getAccount() {
        if(account == null)
        account = [select id, name, numberofemployees, numberoflocations__c from Account
        where id ='0012800000EKMgw']; //:ApexPages.currentPage().getParameters().get('id')];
        return account;
    }
}