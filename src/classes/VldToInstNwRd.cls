public class VldToInstNwRd {    
    public static void ValdInst (List<Opportunity> opty) {                    
        Set<String> nameSet = new Set<String>();
        for(Opportunity opp : [select name from Opportunity]){
            nameSet.add(opp.name);
        }
        for(Opportunity opp : opty){
            if(nameSet.contains(opp.name)){
                opp.addError('error');
            }               
            
        }        
    }
}