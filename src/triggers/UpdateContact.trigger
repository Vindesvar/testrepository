trigger UpdateContact on Account (before update, after update) { 
    
    if(checkRecursiveTriggerClass.firstRun){ 
        checkRecursiveTriggerClass.firstRun = false;
        List<contact> conList = new List<Contact>();
        if(trigger.isUpdate){
            for (Account updatedAccount : [Select Id, Phone, (Select id, name, phone from Contacts) from Account where Id in :Trigger.new]) {            
                for(Contact cont : updatedAccount.Contacts){                
                    cont.phone = updatedAccount.phone; 
                    conList.add(cont);                
                }             
            }
        }
        if(conList.size()>0){
             update conList;         
        }         
    }
}