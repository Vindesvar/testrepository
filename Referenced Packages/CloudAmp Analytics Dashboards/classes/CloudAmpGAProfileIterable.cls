/*
This file is generated and isn't the actual source code for this
managed global class.
This read-only file shows the class's global constructors,
methods, variables, and properties.
To enable code to compile, all methods return null.
*/
global class CloudAmpGAProfileIterable implements System.Iterator<User> {
    global CloudAmpGAProfileIterable() {

    }
    global Boolean hasNext() {
        return null;
    }
    global User next() {
        return null;
    }
    global CloudAmpGA.CloudAmpGAProfileHolder nextProfile(User u) {
        return null;
    }
}
