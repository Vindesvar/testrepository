/*
This file is generated and isn't the actual source code for this
managed global class.
This read-only file shows the class's global constructors,
methods, variables, and properties.
To enable code to compile, all methods return null.
*/
global class CloudAmpGAProfileHolder {
    global String AccountId {
        get;
    }
    global String Country1 {
        get;
    }
    global String Country10 {
        get;
    }
    global String Country2 {
        get;
    }
    global String Country3 {
        get;
    }
    global String Country4 {
        get;
    }
    global String Country5 {
        get;
    }
    global String Country6 {
        get;
    }
    global String Country7 {
        get;
    }
    global String Country8 {
        get;
    }
    global String Country9 {
        get;
    }
    global Datetime CreationDate {
        get;
    }
    global String Goal1 {
        get;
    }
    global String Goal10 {
        get;
    }
    global String Goal11 {
        get;
    }
    global String Goal12 {
        get;
    }
    global String Goal13 {
        get;
    }
    global String Goal14 {
        get;
    }
    global String Goal15 {
        get;
    }
    global String Goal16 {
        get;
    }
    global String Goal17 {
        get;
    }
    global String Goal18 {
        get;
    }
    global String Goal19 {
        get;
    }
    global String Goal2 {
        get;
    }
    global String Goal20 {
        get;
    }
    global String Goal3 {
        get;
    }
    global String Goal4 {
        get;
    }
    global String Goal5 {
        get;
    }
    global String Goal6 {
        get;
    }
    global String Goal7 {
        get;
    }
    global String Goal8 {
        get;
    }
    global String Goal9 {
        get;
    }
    global Decimal GoalsCount {
        get;
    }
    global String Metric1 {
        get;
    }
    global String Metric10 {
        get;
    }
    global String Metric2 {
        get;
    }
    global String Metric3 {
        get;
    }
    global String Metric4 {
        get;
    }
    global String Metric5 {
        get;
    }
    global String Metric6 {
        get;
    }
    global String Metric7 {
        get;
    }
    global String Metric8 {
        get;
    }
    global String Metric9 {
        get;
    }
    global String Name {
        get;
    }
    global Id ObjectId {
        get;
    }
    global String ProfileId {
        get;
    }
    global Double ProfileNumber {
        get;
    }
    global String State1 {
        get;
    }
    global String State10 {
        get;
    }
    global String State2 {
        get;
    }
    global String State3 {
        get;
    }
    global String State4 {
        get;
    }
    global String State5 {
        get;
    }
    global String State6 {
        get;
    }
    global String State7 {
        get;
    }
    global String State8 {
        get;
    }
    global String State9 {
        get;
    }
    global String WebPropertyId {
        get;
    }
    global CloudAmpGAProfileHolder(CloudAmpGA__CloudAmp_Google_Profile__c profile) {

    }
}
