<?xml version="1.0" encoding="UTF-8"?>
<CustomApplication xmlns="http://soap.sforce.com/2006/04/metadata">
    <defaultLandingTab>Google_Analytics_Dash_Settings</defaultLandingTab>
    <description>CloudAmp Analytics Dashboards gives you dashboards with key Google Analytics metrics, right inside of Salesforce, and automatically imports and stores analytics data natively inside Salesforce.</description>
    <label>CloudAmp Analytics Dashboards</label>
    <logo>CloudAmp_Documents/CloudAmp_Logo.png</logo>
    <tab>Google_Analytics_Dash_Settings</tab>
    <tab>CloudAmpGA_Metrics__c</tab>
    <tab>standard-report</tab>
    <tab>standard-Dashboard</tab>
    <tab>Funnel</tab>
    <tab>JituLightening__Integrations</tab>
    <tab>JituLightening__Review__c</tab>
</CustomApplication>
